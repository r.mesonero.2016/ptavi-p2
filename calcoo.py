#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calculadora():
    def __init__(self, ope1, ope2):
        self.ope1 = ope1
        self.ope2 = ope2

    def plus(self):
        return self.ope1 + self.ope2

    def minus(self):
        return self.ope1 - self.ope2


if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")
    calc = Calculadora(operando1, operando2)
    if sys.argv[2] == "suma":
        resultado = calc.plus()
    elif sys.argv[2] == "resta":
        resultado = calc.minus()
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')
    print(resultado)
