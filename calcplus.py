import sys
import csv
import calcoohija
import calcoo


fichero = open(sys.argv[1])
lineas = fichero.readlines()
for info in lineas:
    separador = info.split(",")
    operador = separador[0]
    lista_numeros = list(map(int, separador[1:]))
    if operador == "suma":
        resultado = lista_numeros[0] + lista_numeros[1]
        for lista in lista_numeros[2:]:
            heredero = calcoohija.CalculadoraHija(resultado, lista)
            resultado = heredero.plus()
        print(resultado)
    if operador == "resta":
        resultado = lista_numeros[0] - lista_numeros[1]
        for lista in lista_numeros[2:]:
            heredero = calcoohija.CalculadoraHija(resultado, lista)
            resultado = heredero.minus()
        print(resultado)
    if operador == "multiplica":
        resultado = lista_numeros[0] * lista_numeros[1]
        for lista in lista_numeros[2:]:
            heredero = calcoohija.CalculadoraHija(resultado, lista)
            resultado = heredero.multiplicar()
        print(resultado)
    if operador == "divide":
        if lista_numeros[0] == 0 or lista_numeros[1] == 0:
            print("Division by zero is not allowed")
        else:
            resultado = lista_numeros[0] / lista_numeros[1]
        for lista in lista_numeros[2:]:
            if lista == 0:
                print("Division by zero is not allowed")
            else:
                heredero = calcoohija.CalculadoraHija(resultado, lista)
                resultado = heredero.dividir()
        print(resultado)
fichero.close()




    
