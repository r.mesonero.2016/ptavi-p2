import sys
from calcoo import Calculadora


class CalculadoraHija(Calculadora):
    def multiplicar(self):
        return self.ope1 * self.ope2

    def dividir(self):
        if self.ope2 == 0:
            print("Division by zero is not allowed")
        else:
            return self.ope1 / self.ope2

if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")
    calc = Calculadora(operando1, operando2)
    calchija = CalculadoraHija(operando1, operando2)
    if sys.argv[2] == "suma":
        resultado = calc.plus()
    elif sys.argv[2] == "resta":
        resultado = calc.minus()
    elif sys.argv[2] == "multiplica":
        resultado = calchija.multiplicar()
    elif sys.argv[2] == "divide":
        resultado = calchija.dividir()
    else:
        sys.exit('Operación sólo puede sumar, restar, multiplicar o dividir')
    print(resultado)
